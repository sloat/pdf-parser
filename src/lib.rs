#[macro_use]
extern crate nom;
extern crate flate2;

mod objects;
mod parser;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
