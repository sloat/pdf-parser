use nom;

named!(parse_comment,
    do_parse!(
        tag!(b"%") >>
        take_until_and_consume!("\r\n") >>
        (b"")
    )
);

named!(parse_version<&[u8], f32>,
    do_parse!(
        tag!(b"%PDF-") >>
        take_until_and_consume!("\r\n") >>
        version: call!(nom::float) >>
        (version)
    )
);


