pub mod document;
pub mod page;
pub mod xref;

use std::collections::HashMap;

#[derive(Debug,PartialEq,Eq)]
pub enum PDFObject<'a> {
    Bool(bool),
    Number(&'a str),
    String(&'a str),
    HexString(&'a str),
    Name(&'a str),
    Array(Vec<PDFObject<'a>>),
    Dictionary(HashMap<&'a str, PDFObject<'a>>),
    Stream(&'a [u8]),
    Null,
}


#[derive(Debug)]
pub enum PDFFilter {
    ASCIIHexDecode,
    ASCII85Decode,
    LZWDecode,
    FlateDecode,
    RunLengthDecode,
    CCITTFaxDecode,
    JBIG2Decode,
    DCTDecode,
    JPXDecode,
    Crypt,
}


